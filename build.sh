#!/bin/sh
set -u
set -e
die() {
	echo "error: $*" 1>&2
	exit 254
}
test "$#" = 2 || die "usage: $0 <architecture> <package>"
HOST_ARCH="$1"
PACKAGE="$2"
if ! dpkg-architecture "-a$HOST_ARCH" >/dev/null 2>&1; then
	die "$HOST_ARCH is not a valid architecture"
fi
BUILDTMP=$(mktemp -d) || die "failed to create temporary directory"
cleanup() {
	rm -R -f "$BUILDTMP"
}
trap cleanup EXIT
cd "$BUILDTMP" || die "failed to cd to tmpdir"
export SBUILD_CONFIG="$BUILDTMP/sbuild.conf"
cat >"$SBUILD_CONFIG" <<'EOF'
$build_arch_any = 1;
$build_arch_all = 0;
$build_source = 0;
$distribution = "unstable";
$build_profiles = "cross nocheck";
$manual_depends = ["libc-dev", "libstdc++-dev"];
$dpkg_buildpackage_user_options = ["--jobs-try=1"];
$bd_uninstallable_explainer = "apt";
$source_only_changes = 0;
$apt_update = 1;
$apt_distupgrade = 1;
$lintian_opts = ["-T", "binary-from-other-architecture,triplet-dir-and-architecture-mismatch", "--fail-on", "error"];
$lintian_require_success = 1;
$run_lintian = 1;
$run_autopkgtest = 0;
$run_piuparts = 0;
$sbuild_mode = "user";
1;
EOF
RET=0
sbuild "--host=$HOST_ARCH" "$PACKAGE" >/dev/null || RET=$?
for f in *.build; do
	test -L "$f" && continue
	test -f "$f" || continue
	xz -9c "$f"
done
exit "$RET"
