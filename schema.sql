-- SPDX-License-Identifier: GPL-2.0+

CREATE TABLE depstate (
	source TEXT NOT NULL,
	version TEXT NOT NULL,
	buildarch TEXT NOT NULL,
	hostarch TEXT NOT NULL,
	satisfiable BOOLEAN NOT NULL CHECK (satisfiable in (0, 1)),
	reason TEXT,
	UNIQUE (source, buildarch, hostarch, version));

CREATE TABLE depcheck (
	buildarch TEXT NOT NULL,
	hostarch TEXT NOT NULL,
	releasetime TIMESTAMP NOT NULL,
	updatetime TIMESTAMP NOT NULL,
	giveback BOOLEAN NOT NULL CHECK (giveback in (0, 1)),
	UNIQUE (buildarch, hostarch));
INSERT INTO depcheck (buildarch, hostarch, releasetime, updatetime,
		      giveback) VALUES
	('amd64', 'arm64', '2000-01-01 00:00:00', '2000-01-01 00:00:00', 1),
	('amd64', 'armel', '2000-01-01 00:00:00', '2000-01-01 00:00:00', 1),
	('amd64', 'armhf', '2000-01-01 00:00:00', '2000-01-01 00:00:00', 1),
	('amd64', 'mips64el', '2000-01-01 00:00:00', '2000-01-01 00:00:00', 1),
	('amd64', 'mipsel', '2000-01-01 00:00:00', '2000-01-01 00:00:00', 1),
	('amd64', 'ppc64el', '2000-01-01 00:00:00', '2000-01-01 00:00:00', 1),
	('amd64', 's390x', '2000-01-01 00:00:00', '2000-01-01 00:00:00', 1);

CREATE TABLE builds (
	source TEXT NOT NULL,
	version TEXT NOT NULL,
	buildarch TEXT NOT NULL,
	hostarch TEXT NOT NULL,
	success BOOLEAN NOT NULL CHECK (success in (0, 1)),
	starttime TIMESTAMP NOT NULL,
	filename TEXT NOT NULL);
CREATE INDEX builds_source_index ON builds(source);

CREATE TABLE buildrequests (
	id INTEGER PRIMARY KEY,
	source TEXT NOT NULL,
	buildarch TEXT,
	hostarch TEXT,
	requesttime TIMESTAMP NOT NULL,
        priority INTEGER NOT NULL DEFAULT 0);

CREATE TABLE bugs (
	kind TEXT NOT NULL CHECK (kind in ('bdsat', 'ftbfs', 'ftcbfs')),
	bugnum INTEGER NOT NULL,
	package TEXT,
	affects TEXT NOT NULL,
	title TEXT,
	patched BOOLEAN NOT NULL CHECK (patched in (0, 1)));
CREATE INDEX bugs_affects_index ON bugs(affects);

CREATE TABLE building (
	source TEXT NOT NULL,
	buildarch TEXT NOT NULL,
	hostarch TEXT NOT NULL,
	pid INTEGER NOT NULL);
