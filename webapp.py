#!/usr/bin/python3
# SPDX-License-Identifier: GPL-2.0+

import datetime
import lzma

import apt_pkg
apt_pkg.init()
version_compare = apt_pkg.version_compare

import flask
import flask_sqlalchemy
import jinja2
import sqlalchemy
import werkzeug
import werkzeug.security


app = flask.Flask("crossqa")
app.config["SQLALCHEMY_DATABASE_URI"] = 'sqlite:///db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = flask_sqlalchemy.SQLAlchemy(app)

index_template = """<!DOCTYPE html>
<html>
 <head>
  <title>Debian cross build quality assurance</title>
 </head>
 <body>
  <header>
   <h1>Debian cross build quality assurance</h1>
  </header>
  <section>
   <h3>Recently failed builds</h3>
   <table>
    <thead>
     <tr>
      <th>source</th>
      <th>version</th>
      <th>build architecture</th>
      <th>host architecture</th>
      <th>started</th>
      <th>result log</th>
      <th>bugs</th>
     </tr>
    </thead>
    <tbody>
     {%- for build in builds|sort(attribute='starttime', reverse=true) -%}
      <tr>
       <td>
        <a href="{{ url_for("show_source", source=build.source) }}">
         {{- build.source|e -}}
        </a>
       </td>
       <td>{{ build.version|e }}</td>
       <td>{{ build.buildarch|e }}</td>
       <td>{{ build.hostarch|e }}</td>
       <td>
        {{- build.starttime|sqltimestamp|formatts -}}
       </td>
       <td>
        <a href="{{ url_for("show_log", filename=build.filename[:-3]) }}">log</a>
        <a href="{{ url_for("show_log", filename=build.filename) }}">xz</a>
       </td>
       <td>
        {%- if build.buglvl == 2 -%}
         patch reported
        {%- elif build.buglvl == 1 -%}
         bug reported
        {%- endif -%}
       </td>
      </tr>
     {%- endfor -%}
    </tbody>
   </table>
  </section>
  <footer>
   <h3>Details about this service</h3>
   <ul>
    <li>Maintainer: Helmut Grohne &lt;helmut@subdivi.de&gt;</li>
    <li>Source: git://git.subdivi.de/~helmut/crossqa.git</li>
   </ul>
  </footer>
 </body>
</html>
"""

src_template = """
{%- macro render_bug(bugobj) -%}
 <a href="https://bugs.debian.org/{{ bugobj.bugnum }}">#{{ bugobj.bugnum }}</a>
 {%- if bugobj.patched %}
  [<abbr title="patch available">+</abbr>]
 {%- endif -%}:
 {% if bugobj.package != "src:" + bugobj.affects and
       not bugobj.title.startswith(bugobj.affects + ":") -%}
  {{- bugobj.package|e }}:
 {% endif -%}
 {{- bugobj.title|e -}}
{%- endmacro -%}
<!DOCTYPE html>
<html>
 <head>
  <title>{{ sourcepackage|e }} - Debian cross build</title>
  <style>
tr.dep.bad td:nth-child(1) {
    background-color: #faa;
}
tr.dep.tempbad td:nth-child(1) {
    background-color: #ffa;
}
tr.dep.good td:nth-child(1) {
    background-color: #afa;
}
tr.build.bad td:nth-child(5) {
    background-color: #faa;
}
tr.build.tempbad td:nth-child(5) {
    background-color: #ffa;
}
tr.build.good td:nth-child(5) {
    background-color: #afa;
}
th {
    padding-left: 1em;
    padding-right: 1em;
}
td {
    padding-left: 1px;
    padding-right: 1em;
}
td:last-child {
    padding-right: 1px;
}
footer {
    margin-top: 3em;
    border-top: 1px solid;
}
  </style>
 </head>
 <body>
  <header>
   <h1>
    <a href="https://tracker.debian.org/pkg/{{ sourcepackage|e }}">
     {{- sourcepackage|e -}}
    </a>
   </h1>
  </header>
  {%- if bugs.ftbfs -%}
   <section>
    <h3>Reported <abbr title="fails to build from source">FTBFS</abbr> bugs</h3>
    <ul>
     {%- for bug in bugs.ftbfs|sort(attribute="bugnum") -%}
      <li>
       {{- render_bug(bug) -}}
      </li>
     {%- endfor -%}
    </ul>
   </section>
  {%- endif -%}
  <section>
   <h3>Cross build dependency satisfiability</h3>
   {%- if bugs.bdsat -%}
    <h5>Reported satisfiability problems</h5>
    <ul>
     {%- for bug in bugs.bdsat|sort(attribute="bugnum") -%}
      <li>
       {{- render_bug(bug) -}}
      </li>
     {%- endfor -%}
    </ul>
   {%- endif -%}
   <table>
    <thead>
     <tr>
      <th>state</th>
      <th>architectures</th>
     </tr>
    </thead>
    <tbody>
     {%- set okarchs = depresult.pop(None, None) -%}
     {%- for reason, archs in depresult.items()|sort -%}
      <tr class="dep {{ "tempbad" if reason.startswith("skew") else "bad" }}">
       <td>{{ reason|e }}</td>
       <td>{{ archs|archpairs_format }}</td>
      </tr>
     {%- endfor -%}
     {%- if okarchs -%}
      <tr class="dep good">
       <td>ok</td>
       <td>{{ okarchs|archpairs_format }}</td>
      </tr>
     {%- endif -%}
    </tbody>
   </table>
   {%- if show_debcheck -%}
    <h5>See also</h5>
    <ul>
     {%- if show_bootstrapdn -%}
      <li>
       <a href="https://bootstrap.debian.net/cross_all/{{ sourcepackage|e }}.html">bootstrap.debian.net</a>
      </li>
     {%- endif -%}
     <li>
      <a href="https://qa.debian.org/dose/debcheck/cross_unstable_main_amd64/latest/packages/{{ sourcepackage|e }}.html">debcheck</a>
     </li>
    </ul>
   {%- endif -%}
  </section>
  <section>
   <h3>Cross builds</h3>
   {%- if bugs.ftcbfs -%}
    <h5>Reported cross build failures</h5>
    <ul>
     {%- for bug in bugs.ftcbfs|sort(attribute="bugnum") -%}
      <li>
       {{- render_bug(bug) -}}
      </li>
     {%- endfor -%}
    </ul>
   {%- endif -%}
   {%- if builds -%}
    <table>
     <thead>
      <tr>
       <th>started</th>
       <th>version</th>
       <th>build architecture</th>
       <th>host architecture</th>
       <th>result log</th>
      </tr>
     </thead>
     <tbody>
      {%- for build in builds|sort(attribute='starttime', reverse=true) -%}
       <tr class="build {{ "good" if build.success else "bad" }}">
        <td>
         {{- build.starttime|sqltimestamp|formatts -}}
        </td>
        <td>{{ build.version|e }}</td>
        <td>{{ build.buildarch|e }}</td>
        <td>{{ build.hostarch|e }}</td>
        <td>
         <a href="{{ url_for("show_log", filename=build.filename[:-3]) }}">
          {{- "ok" if build.success else "failed" -}}
         </a>
         <a href="{{ url_for("show_log", filename=build.filename) }}">xz</a>
        </td>
       </tr>
      {%- endfor -%}
     </tbody>
    </table>
   {%- else -%}
    <p>No build performed yet.</p>
   {%- endif -%}
   <form method="POST" action="{{ url_for("request_schedule")|e }}">
    <input type="submit" name="schedule" value="cross build" />
    {{ sourcepackage|e }} for
    <input type="hidden" name="source" value="{{ sourcepackage|e }}" />
    <select name="archpair">
     <option value="any_any">{{ ("any", "any")|archpair_format }}</option>
     {%- for buildarch, hostarch in architectures|sort -%}
      <option value="{{ buildarch|e }}_{{ hostarch|e }}">
       {{- (buildarch, hostarch)|archpair_format -}}
      </option>
     {%- endfor -%}
    </select>
   </form>
  </section>
  <footer>
   <h3>Details about this service</h3>
   <ul>
    <li>Maintainer: Helmut Grohne &lt;helmut@subdivi.de&gt;</li>
    <li>Source: git://git.subdivi.de/~helmut/crossqa.git</li>
   </ul>
  </footer>
 </body>
</html>
"""

schedule_template = """<!DOCTYPE html>
<html>
 <body>
  <p>Scheduled a build of {{ request.form["source"]|e }}
   {%- if buildarch or hostarch %}
    for {{ (buildarch|default("any"), hostarch|default("any"))|archpair_format -}}
   {%- endif %}.
  <p>
 </body>
</html>
"""

@app.template_filter("sqltimestamp")
def sqltimestamp_filter(s):
    strptime = datetime.datetime.strptime
    try:
        return strptime(s, "%Y-%m-%d %H:%M:%S.%f").replace(microsecond=0)
    except ValueError:
        return strptime(s, "%Y-%m-%d %H:%M:%S")


def formatts(ts):
    assert isinstance(ts, datetime.datetime)
    dt = datetime.datetime.utcnow() - ts
    if dt < datetime.timedelta(seconds=1):
        return "now"
    if dt < datetime.timedelta(seconds=100):
        return "%d s" % dt.seconds
    if dt < datetime.timedelta(minutes=100):
        return "%d m" % (dt.seconds // 60)
    if dt < datetime.timedelta(days=1):
        return "%d h" % (dt.seconds // (60 * 60))
    return "%d d" % dt.days


@app.template_filter("formatts")
def formatts_filter(ts):
    return jinja2.utils.markupsafe.Markup(
        '<time title="%s" datetime="%s">%s</time>' % (ts, ts, formatts(ts))
    )

@app.template_filter("archpair_format")
def archpair_format_filter(archpair):
    return jinja2.utils.markupsafe.Markup(
        "%s &rarr; %s" % tuple(map(jinja2.utils.markupsafe.escape, archpair))
    )

def group_pairs(pairs):
    result = {}
    for v, w in pairs:
        result.setdefault(v, set()).add(w)
    return result

def render_archset(subset, all_archs):
    if len(subset) == 1:
        return next(iter(subset))
    if subset == all_archs:
        return "any"
    return "{%s}" % ", ".join(
        map(jinja2.utils.markupsafe.escape, sorted(subset))
    )

@app.template_filter('archpairs_format')
@jinja2.pass_context
def archpairs_format_filter(context, some_archs):
    architectures = group_pairs(context["architectures"])
    fwdmap = {}  # build architecture -> host architecture set representation
    for buildarch, hostarchs in group_pairs(some_archs).items():
        fwdmap[buildarch] = render_archset(hostarchs, architectures[buildarch])
    allbuildarchs = set(architectures.keys())
    # host architecture set representation -> build architecture set
    flippedit = group_pairs((v, k) for (k, v) in fwdmap.items()).items()
    maps = ("%s &rarr; %s" % (render_archset(buildarchs, allbuildarchs),
                              hostarchrep)
            for hostarchrep, buildarchs in flippedit)
    return jinja2.utils.markupsafe.Markup("; ".join(sorted(maps)))

def collect_depstate(conn, source):
    version = None
    depstate = None
    query = sqlalchemy.text("""
        SELECT version, buildarch, hostarch, satisfiable, reason
            FROM depstate WHERE source = :source;""")
    for row in conn.execute(query, source=source):
        if version is None or version_compare(version, row.version) > 0:
            version = row.version
            depstate = {}
        depstate[row.buildarch, row.hostarch] = \
                None if row.satisfiable else row.reason
    if version is None:
        raise werkzeug.exceptions.NotFound()
    depresult = {}
    for archpair, reason in depstate.items():
        depresult.setdefault(reason, set()).add(archpair)
    return version, depresult

@app.route("/")
def show_index():
    with db.engine.connect() as conn:
        builds = list(conn.execute("""
            SELECT source, version, buildarch, hostarch, starttime, filename,
                ifnull((SELECT max(patched + 1) FROM bugs
                                                WHERE affects = source),
                       0) AS buglvl
                FROM builds
                WHERE success = 0
                ORDER BY starttime
                DESC LIMIT 10;"""))
    return flask.render_template_string(index_template, builds=builds)

@app.route("/src/<source>")
def show_source(source):
    context = dict(sourcepackage=source)
    with db.engine.connect() as conn:
        query = sqlalchemy.text("SELECT buildarch, hostarch FROM depcheck;")
        context["architectures"] = set(map(tuple, conn.execute(query)))
        context["version"], context["depresult"] = collect_depstate(conn,
                                                                    source)
        query = sqlalchemy.text("""
            SELECT version, buildarch, hostarch, success, starttime, filename
                FROM builds WHERE source = :source;""")
        context["builds"] = list(conn.execute(query, source=source))
        query = sqlalchemy.text("""
            SELECT bugnum, kind, title, package, patched, affects
                FROM bugs WHERE affects = :affects;""")
        context["bugs"] = {}
        for bug in conn.execute(query, affects=source):
            context["bugs"].setdefault(bug.kind, []).append(bug)
        context["show_bootstrapdn"] = \
                any(reason and not reason.startswith("skew ")
                    for reason in context["depresult"].keys())
        context["show_debcheck"] = \
                any(context["depresult"].keys())
    return flask.render_template_string(src_template, **context)

@app.route("/build/<path:filename>")
def show_log(filename):
    if filename.endswith(".xz"):
        return flask.send_from_directory("logs", filename,
                                         mimetype="application/octet-stream")
    filename = werkzeug.security.safe_join("logs", filename + ".xz")
    try:
        return flask.send_file(lzma.open(filename, "rb"),
                               mimetype="text/plain")
    except FileNotFoundError:
        raise werkzeug.exceptions.NotFound()


@app.route("/schedule", methods=["POST"])
def request_schedule():
    source = flask.request.form["source"]
    try:
        buildarch, hostarch = flask.request.form["archpair"].split("_")
    except ValueError:
        raise werkzeug.exceptions.BadRequest()
    if buildarch == "any":
        buildarch = None
    if hostarch == "any":
        hostarch = None
    with db.engine.connect() as conn:
        query = sqlalchemy.text("""
            SELECT 1 FROM depstate WHERE source = :source;""")
        if not conn.execute(query, source=source).first():
            raise werkzeug.exceptions.BadRequest()
        query = sqlalchemy.text("""
            SELECT 1 FROM depcheck
                WHERE buildarch = ifnull(:buildarch, buildarch)
                    AND hostarch = ifnull(:hostarch, hostarch);""")
        if not conn.execute(query, buildarch=buildarch,
                            hostarch=hostarch).first():
            raise werkzeug.exceptions.BadRequest()
        query = sqlalchemy.text("""
            INSERT INTO buildrequests (source, buildarch, hostarch,
                                       requesttime)
                VALUES (:source, :buildarch, :hostarch, datetime('now'));""")
        conn.execute(query, source=source, buildarch=buildarch,
                     hostarch=hostarch)
    return flask.render_template_string(schedule_template, buildarch=buildarch,
                                        hostarch=hostarch)
